﻿using System;
using System.Collections.Generic;

namespace FizzBuzz
{
    public class FizzBuzzService
    {
        public static void PrintFizzBuzz(int x)
        {
            List<object> fizzBuzzResult = FizzBuzzService.FizzBuzz(x);
            fizzBuzzResult.ForEach(element => Console.WriteLine(element));
        }
        //Kiss/Yagni/Dry - not always easy to keep it in balance.
        public static List<object> FizzBuzz(int x)
        {
            List<object> resultList = new List<object>();

            for (int i = 1; i <= x; i++)
            {
                bool fizz = i % 3 == 0;
                bool buzz = i % 5 == 0;

                if (fizz && buzz)
                    resultList.Add("fizzbuzz");
                else
                if (fizz)
                    resultList.Add("fizz");
                else
                if (buzz)
                    resultList.Add("buzz");
                else
                    resultList.Add(i);
            }
            return resultList;
        }
    }
}
