﻿using FizzBuzz;
using System;

namespace FuzzBuzz
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please insert the number you want to FizzBuzz!");
            var p = Console.ReadLine();
            int x;
            while (!int.TryParse(p, out x) || x <= 0)
            {
                Console.WriteLine($"Your input {x} is invalid. Please insert a positive integer number!");
                p = Console.ReadLine();
            }
            FizzBuzzService.PrintFizzBuzz(x);
            Console.ReadLine(); 
        }
    }
}
