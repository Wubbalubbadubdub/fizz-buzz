﻿using System.Collections.Generic;
using FizzBuzz;
using NUnit.Framework;

namespace FizzBuzzTests
{

    [TestFixture]
    public class FizzBuzzServiceTests
    {
        [Test]
        public void Can_Create_Instance()
        {
            FizzBuzzService fizzBuzzService = new FizzBuzzService();
            Assert.IsNotNull(fizzBuzzService);
        }
        //This skill test was in time when i am probably changing the way  i name tests
        //Previously i went for methodname_paramenters_expectedresult
        //but i think i like more when they are named by behaviour.
        [TestCaseSource("FizzBuzzCases")]
        public List<object> Can_Fizz_Buzz(int endNumber)
        {
            return FizzBuzzService.FizzBuzz(endNumber);
        }

        private static IEnumerable<TestCaseData> FizzBuzzCases
        {
            get
            {
                yield return new TestCaseData(2).Returns(
                    new List<object> { 1, 2 });
                yield return new TestCaseData(15).Returns(
                    new List<object> { 1, 2, "fizz", 4, "buzz", "fizz", 7, 8, "fizz", "buzz", 11, "fizz", 13, 14, "fizzbuzz" });
                yield return new TestCaseData(30).Returns(
                    new List<object> { 1, 2, "fizz", 4, "buzz", "fizz", 7, 8, "fizz", "buzz", 11, "fizz", 13, 14, "fizzbuzz", 16, 17, "fizz",
                        19, "buzz", "fizz", 22, 23, "fizz", "buzz", 26, "fizz", 28, 29, "fizzbuzz"});
                yield return new TestCaseData(-1).Returns(
                  new List<object> {});
            }
        }
    }
}
